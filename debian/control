Source: raku-file-which
Maintainer: Debian Rakudo Maintainers <pkg-rakudo-devel@lists.alioth.debian.org>
Uploaders: Dominique Dumont <dod@debian.org>
Section: interpreters
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-raku,
               rakudo (>= 2022.07-2)
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl6-team/modules/raku-file-which
Vcs-Git: https://salsa.debian.org/perl6-team/modules/raku-file-which.git
Homepage: https://github.com/azawawi/perl6-file-which
Rules-Requires-Root: no

Package: raku-file-which
Architecture: any
Depends: ${misc:Depends},
         ${raku:Depends},
         rakudo (>= 2022.07-2)
Description: Cross platform Perl 6 executable path finder (aka which on UNIX)
 File::Which finds the full or relative paths to an executable program on
 the system. This is normally the function of which utility which is
 typically implemented as either a program or a built in shell command.
 On some unfortunate platforms, such as Microsoft Windows it is not
 provided as part of the core operating system.
 .
 This module provides a consistent API to this functionality regardless
 of the underlying platform.
